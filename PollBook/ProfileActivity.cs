﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Net;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.Design.Widget;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace PollBook
{
    /// <summary>
    /// Profile page activity class
    /// </summary>
    [Activity(Label = "Poll_Statistic_Android", MainLauncher = false, Icon = "@drawable/ic_launcher", Theme = "@style/MyTheme")]
    class ProfileActivity : PollActivity
    {
        //Layouts
        private LinearLayout layout;
        private ImageView profileImage;
        /// <summary>
        /// Main function. Runs automatically on app start
        /// </summary>
        /// <param name="bundle"></param>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            SetTheme(UserSettings.theme);

            SetContentView(Resource.Layout.Profile);

            StaticData.CurrentActivity = this;

            //Getting access for main layout
            layout = FindViewById<LinearLayout>(Resource.Id.profile_layout);


            //Getting access for main toolbar
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.Profile);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            //Getting access for recyclerView
            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            pollData = new List<Poll>();
            adapter = new PollAdapter(this, pollData);
            recyclerView.SetAdapter(adapter);


            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(layoutManager);

            //Getting access for refresher
            refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            refresher.SetColorSchemeColors(Resource.Color.darkblue,
                                      Resource.Color.purple,
                                      Resource.Color.orange,
                                      Resource.Color.green);
            refresher.Refresh += async delegate
            {
                await getServerData();
                refresher.Refreshing = false;
                ShowFeedAsync();
            };

            var user = JsonConvert.DeserializeObject<Users>(UserSettings.User);
            var nameView = FindViewById<TextView>(Resource.Id.textView);
            nameView.Text = user.firstName + " " + user.secondName;

            profileImage = FindViewById<ImageView>(Resource.Id.profileImage);
            if (JsonConvert.DeserializeObject<Users>(UserSettings.User).imageUrl != "")
            {
                profileImage.SetImageBitmap(GetImageBitmapFromUrl(JsonConvert.DeserializeObject<Users>(UserSettings.User).imageUrl));
            }
           
            profileImage.Click += ProfileImage_Click;

            //Sync data with server & show feed (async method)
            syncData();
          
        }

        private void ProfileImage_Click(object sender, EventArgs e)
        {
            Intent = new Intent();
            Intent.SetType("image/*");
            Intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(Intent, "Select Picture"), 9999);
        }

        protected override async void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if ((requestCode == 9999) && (resultCode == Result.Ok) && (data != null))
            {
                var uri = data.Data;
                profileImage.SetImageURI(uri);

                var user = JsonConvert.DeserializeObject<Users>(UserSettings.User);
                user.imageUrl = await UploadImage(ContentResolver.OpenInputStream(uri));
                await mobileService.GetTable<Users>().UpdateAsync(user);
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.profile_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    StartActivity(typeof(MainActivity));
                    break;
                case Resource.Id.menu_search:

                    var searchEdit = new EditText(this);
                    Android.App.AlertDialog alertDialog;
                    var builder = new Android.App.AlertDialog.Builder(this);
                    builder.SetTitle(GetString(Resource.String.Search)).SetPositiveButton("OK", (senderAlert, args) => Search(searchEdit.Text)).SetNegativeButton(GetString(Resource.String.cancel), (senderAlert, args) => alertDialog = null); ;

                    alertDialog = builder.Create();
                    alertDialog.SetView(searchEdit);
                    alertDialog.Show();
                    break;
                case Resource.Id.menu_logout:
                    StartActivity(typeof(LoginActivity));
                    break;

            }
            return base.OnOptionsItemSelected(item);
        }


        /// <summary>
        /// Showing cards with options on feed layout
        /// </summary>
        /// <returns></returns>
        public override void ShowFeedAsync()
        {
            pollData.Clear();
            foreach (var dataCollum in serverData)
            {
                Poll currentPoll = new Poll();

                var users = JsonConvert.DeserializeObject<Users>(UserSettings.User);

                var userPolls = JsonConvert.DeserializeObject<List<string>>(users.userPolls);
                if (userPolls != null)
                {
                    if (userPolls.Find(x => x == dataCollum.Id) != null)
                    {
                        //Parsing data
                        currentPoll.pollTitle = dataCollum.pollTitle;
                        currentPoll.pollRate = dataCollum.pollRate;
                        currentPoll.ID = dataCollum.Id;
                        currentPoll.options = new List<Option>();
                        currentPoll.ownerId = dataCollum.ownerId;

                        int index = 0;
                        if (tryGetUserVote(currentPoll.ID, ref index))
                            currentPoll.isVoted = true;

                        //Convering string that contains options options titles into list
                        foreach (var option in JsonConvert.DeserializeObject<List<Option>>(dataCollum.pollOptions))
                        {
                            currentPoll.options.Add(option);
                        }
                        pollData.Add(currentPoll);
                    }
                }              
            }
            adapter = new PollAdapter(this, pollData);
            recyclerView.SetAdapter(adapter);
            adapter.NotifyDataSetChanged();
        }
      

        public async Task<string> UploadImage(Stream source)
        {
            var client = new ImgurClient(GetString(Resource.String.imgurClientId), GetString(Resource.String.imgurClientSecret));
            var endpoint = new ImageEndpoint(client);

            try
            {
                var image = await endpoint.UploadImageStreamAsync(source);

                return image.Link;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            };
        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }


    }

}
