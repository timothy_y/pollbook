using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using Environment = System.Environment;

namespace PollBook
{
    [Activity(Label = "PollActivity")]
    class PollActivity : AppCompatActivity
    {
        public DrawerLayout drawerLayout;
        public NavigationView navigationView;

        public RecyclerView recyclerView;
        public RecyclerView.LayoutManager layoutManager;

        public PollAdapter adapter;
        public SwipeRefreshLayout refresher;

        //Vars for work with server part
        public MobileServiceClient mobileService = CurrentUser.mobileService;
        public List<Polls> serverData;
        public List<Poll> pollData;
        public List<Users> users;

        /// <summary>
        /// Getting access to azure server & getting polls info table (asunc method)
        /// </summary>
        /// <returns></returns>
        public async Task<bool> getServerData()
        {
            MobileServiceInvalidOperationException exeption = (MobileServiceInvalidOperationException)null;
            try
            {
                serverData = await mobileService.GetTable<Polls>().ToListAsync();

                serverData.Reverse();

                users = await mobileService.GetTable<Users>().ToListAsync();

                var user =
                    await mobileService.GetTable<Users>()
                        .Where(
                            x =>
                                x.userId ==
                                JsonConvert.DeserializeObject<Users>(UserSettings.User).userId)
                        .ToListAsync();

                UserSettings.User = JsonConvert.SerializeObject(user.Last());

            }
            catch (MobileServiceInvalidOperationException ex)
            {
                exeption = ex;
            }
            return exeption == null;
        }


        /// <summary>
        /// Showing cards with options on feed layout
        /// </summary>
        /// <returns></returns>
        public virtual void ShowFeedAsync()
        {
            pollData.Clear();
            foreach (var dataCollum in serverData)
            {
                Poll currentPoll = new Poll();

                //Parsing data
                currentPoll.pollTitle = dataCollum.pollTitle;
                currentPoll.pollRate = dataCollum.pollRate;
                currentPoll.ID = dataCollum.Id;
                currentPoll.options = new List<Option>();
                currentPoll.ownerId = dataCollum.ownerId;

                int index = 0;
                if (tryGetUserVote(currentPoll.ID, ref index))
                    currentPoll.isVoted = true;

                //Convering string that contains options options titles into list
                foreach (var option in JsonConvert.DeserializeObject<List<Option>>(dataCollum.pollOptions))
                {
                    currentPoll.options.Add(option);
                }
                pollData.Add(currentPoll);
            }
            adapter = new PollAdapter(this, pollData);
            recyclerView.SetAdapter(adapter);
            adapter.NotifyDataSetChanged();

        }

        public bool isOnline()
        {
            if (((Android.Net.ConnectivityManager)GetSystemService(ConnectivityService)).ActiveNetwork == null)
                return false;
            else return ((Android.Net.ConnectivityManager)GetSystemService(ConnectivityService)).ActiveNetworkInfo.IsConnectedOrConnecting;

        }

        /// <summary>
        /// Method that call outputting feed and show progress dialog on screen. (async method)
        /// </summary>
        public async void syncData()
        {
            if (isOnline())
            {
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.SetTitle(GetString(Resource.String.Sync));
                progressDialog.SetCancelable(false);
                progressDialog.SetCanceledOnTouchOutside(false);
                progressDialog.Show();

                await getServerData();

                ShowFeedAsync();

                progressDialog.Cancel();

            }
            else
            {
                var builder = new Android.App.AlertDialog.Builder(this);
                builder.SetMessage(GetString(Resource.String.networkError)).SetNeutralButton("OK", (senderAlert, args) => System.Environment.Exit(0));

                var alertDialog = builder.Create();
                alertDialog.Show();
            }
        }


        /// <summary>
        /// Update & send info of voted option ro server
        /// </summary>
        /// <param name="pollIndex"></param>
        /// <param name="optionIndex"></param>
        /// <returns></returns>
        public async Task updateOptionRate(string ID, int optionIndex)
        {
            await addVotedPoll(ID, optionIndex);
            foreach (Polls poll in serverData)
                if (poll.Id == ID)
                {
                    var options = JsonConvert.DeserializeObject<List<Option>>(poll.pollOptions);
                    options[optionIndex].optionRate++;
                    poll.pollRate++;
                    poll.pollOptions = JsonConvert.SerializeObject(options);

                    //Sending info to server
                    await mobileService.GetTable<Polls>().UpdateAsync(poll);
                    break;
                }
        }

        /// <summary>
        /// Save info of user voted polls
        /// </summary>
        /// <param name="ID"></param>
        public async Task addVotedPoll(string ID, int optionIndex)
        {
            var users =
                await mobileService.GetTable<Users>()
                        .Where(
                            x =>
                                x.userId ==
                                JsonConvert.DeserializeObject<Users>(UserSettings.User).userId)
                        .ToListAsync();

            List<UserPoll> votedPolls;
            if (users.Last().votedPolls == "")
            {
                votedPolls = new List<UserPoll>();
                votedPolls.Add(new UserPoll()
                {
                    ID = ID,
                    optionIndex = optionIndex
                });
            }
            else
            {
                votedPolls = JsonConvert.DeserializeObject<List<UserPoll>>(users.Last().votedPolls);
                votedPolls.Add(new UserPoll()
                {
                    ID = ID,
                    optionIndex = optionIndex
                });
            }
            users.Last().votedPolls = JsonConvert.SerializeObject(votedPolls);

            await mobileService.GetTable<Users>().UpdateAsync(users.Last());
        }

        public async Task addUserPoll(string ID)
        {
            var users =
                 await mobileService.GetTable<Users>()
                        .Where(
                            x =>
                                x.userId ==
                                JsonConvert.DeserializeObject<Users>(UserSettings.User).userId)
                        .ToListAsync();

            List<string> userPolls;
            if (users.Last().userPolls == "")
            {
                userPolls = new List<string>();
                userPolls.Add(ID);
            }
            else
            {
                userPolls = JsonConvert.DeserializeObject<List<string>>(users.Last().userPolls);
                userPolls.Add(ID);
            }
            users.Last().userPolls = JsonConvert.SerializeObject(userPolls);

            await mobileService.GetTable<Users>().UpdateAsync(users.Last());
        }

        public async Task removeUserPoll(string ID)
        {
            var users = JsonConvert.DeserializeObject<Users>(UserSettings.User);

            var userPolls = JsonConvert.DeserializeObject<List<string>>(users.userPolls);

            userPolls.Remove(userPolls.Find(x => x == ID));

            users.userPolls = JsonConvert.SerializeObject(userPolls);

            await mobileService.GetTable<Users>().UpdateAsync(users);
        }

        /// <summary>
        /// Remove info of user voted polls
        /// </summary>
        /// <param name="ID"></param>
        public async Task removeVotedPoll(string ID)
        {
            var users = JsonConvert.DeserializeObject<Users>(UserSettings.User);

            var votedPolls = JsonConvert.DeserializeObject<List<UserPoll>>(users.votedPolls);

            votedPolls.Remove(votedPolls.Find(x => x.ID == ID));

            users.votedPolls = JsonConvert.SerializeObject(votedPolls);

            await mobileService.GetTable<Users>().UpdateAsync(users);
        }

        public bool tryGetUserVote(string ID, ref int index)
        {
            var users = JsonConvert.DeserializeObject<Users>(UserSettings.User);
            var votedPolls = JsonConvert.DeserializeObject<List<UserPoll>>(users.votedPolls);
            if (votedPolls != null)
            {
                var votedPoll = votedPolls.Find(x => x.ID == ID);
                if (votedPoll != null)
                {
                    index = votedPoll.optionIndex;
                    return true;
                }
                else return false;
            }
            else return false;
        }

        private void CreateAndShowDialog(Exception exception, String title)
        {
            CreateAndShowDialog(exception.Message, title);
        }

        private void CreateAndShowDialog(string message, string title)
        {
            var builder = new Android.App.AlertDialog.Builder(this);

            builder.SetMessage(message);
            builder.SetTitle(title);
            builder.Create().Show();
        }

        public async void Search(string param)
        {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.SetTitle(Resource.String.Search);
            progressDialog.Show();

            await getServerData();

            foreach (Polls dataCollum in serverData)
            {
                if (dataCollum.pollTitle.Contains(param) || dataCollum.pollOptions.Contains(param))
                {
                    Poll currentPoll = new Poll();

                    //Parsing data
                    currentPoll.pollTitle = dataCollum.pollTitle;
                    currentPoll.pollRate = dataCollum.pollRate;
                    currentPoll.ID = dataCollum.Id;
                    currentPoll.options = new List<Option>();

                    //Convering string that contains options options titles into list
                    foreach (var option in JsonConvert.DeserializeObject<List<Option>>(dataCollum.pollOptions))
                    {
                        currentPoll.options.Add(option);
                    }

                    //Checking if user already vote for this options
                    currentPoll.isVoted = VotedPolls.yourVotedPolls.Contains(dataCollum.Id);
                    pollData.Add(currentPoll);
                }
            }
            adapter = new PollAdapter(this, pollData);
            recyclerView.SetAdapter(adapter);
            adapter.NotifyDataSetChanged();

            progressDialog.Cancel();

        }
    }
}