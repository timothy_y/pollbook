using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.Design.Widget;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace PollBook
{
    [Activity(Icon = "@drawable/ic_launcher", Label = "Poll Statistic", MainLauncher = false, Theme = "@style/MyTheme")]
    class EditActivity : AppCompatActivity
    {
        //Var for work with server part
        private static MobileServiceClient mobileService = CurrentUser.mobileService;

        //Layouts and views
        private LinearLayout layout;
        private EditText pollTitleEdit;
        private List<EditText> pollOptionsEdit = new List<EditText>();
        private LinearLayout optionsLayout;
        private CardView optionsCardView;
        private int optionsCount = 2;
        private Poll currentPoll;

        /// <summary>
        /// Main function. Runs automatically on app start
        /// </summary>
        /// <param name="bundle"></param>
        protected  override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            currentPoll = JsonConvert.DeserializeObject<Poll>(Intent.GetStringExtra("poll"));

            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            SetTheme(UserSettings.theme);

            SetContentView(Resource.Layout.AddPoll);

            //Getting access for main layout
            layout = FindViewById<LinearLayout>(Resource.Id.addPage_layout);
            layout.SetPadding(50, 50, 50, 50);

            //Getting access for main toolbar
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.Add);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);



            //Creating card with options title text field
            CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };
            LinearLayout tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            tempLayout.SetPadding(20, 20, 20, 20);


            //Creating card with 2 default options options text fields
            tempLayout.AddView(new TextView(this) { Text = GetString(Resource.String.titleInput), TextSize = 20 });
            pollTitleEdit = new EditText(this) { Text = currentPoll.pollTitle, InputType = Android.Text.InputTypes.ClassText };

            //Adding this all to main layout
            tempLayout.AddView(pollTitleEdit);
            cardView.AddView(tempLayout);
            layout.AddView(cardView);


            optionsCardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };
            optionsLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            optionsLayout.SetPadding(20, 20, 20, 20);

            foreach (var option in currentPoll.options)
            {
                pollOptionsEdit.Add(new EditText(this) {Text = option.optionsTitle, InputType = Android.Text.InputTypes.ClassText});
                optionsLayout.AddView(pollOptionsEdit.Last());
            }

            //Adding this all to main layout
            optionsCardView.AddView(optionsLayout);
            layout.AddView(optionsCardView);


            //Creating card with options settings
            CardView settingsCard = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };
            LinearLayout settingsLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            settingsLayout.SetPadding(20, 20, 20, 20);

            settingsLayout.AddView(new TextView(this) { Text = GetString(Resource.String.optionsSettings), TextSize = 20 });

            Android.Widget.Switch anonimSwitch = new Android.Widget.Switch(this) { Text = GetString(Resource.String.anonimPoll), TextSize = 15 };
            anonimSwitch.SetTextColor(Android.Graphics.Color.LightGray);
            settingsLayout.AddView(anonimSwitch);

            Android.Widget.Switch reVoteSwitch = new Android.Widget.Switch(this) { Text = GetString(Resource.String.revoteOption), TextSize = 15 };
            reVoteSwitch.SetTextColor(Android.Graphics.Color.LightGray);
            settingsLayout.AddView(reVoteSwitch);

            //Adding this all to main layout
            settingsCard.AddView(settingsLayout);
            layout.AddView(settingsCard);


        }

        /// <summary>
        /// Filling toolbar with buttons from .xml file
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.addPage_top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        /// <summary>
        /// Handling toolbar buttons clicks
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    StartActivity(typeof(MainActivity));
                    break;
                case Resource.Id.menu_add:
                    pollOptionsEdit.Add(new EditText(this) { Hint = GetString(Resource.String.optionsHint), InputType = Android.Text.InputTypes.ClassText });
                    optionsCount++;
                    optionsLayout.AddView(pollOptionsEdit.Last());
                    break;
                case Resource.Id.menu_remove:
                    if (!pollOptionsEdit.Last().Equals(pollOptionsEdit.First()))
                    {
                        optionsLayout.RemoveView(pollOptionsEdit.Last());
                        pollOptionsEdit.Remove(pollOptionsEdit.Last());
                        optionsCount--;
                    }
                    break;
                case Resource.Id.menu_send:

                    sendPoll();

                    return true;

            }
            return base.OnOptionsItemSelected(item);
        }

        /// <summary>
        /// Sending options info to server
        /// </summary>
        private async void sendPoll()
        {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.SetTitle(GetString(Resource.String.Send));
            progressDialog.SetCancelable(false);
            progressDialog.SetCanceledOnTouchOutside(false);
            progressDialog.Show();

            var options = new List<Option>();

            //Converting all options titles into one string to save it on server
            pollOptionsEdit.ForEach(x =>
            {
                options.Add(new Option()
                {
                    optionsTitle = x.Text,
                    optionRate = 0
                });
            });

            //Sending data to server

            CurrentPlatform.Init();
            await mobileService.GetTable<Polls>().InsertAsync(new Polls
            {
                pollTitle = pollTitleEdit.Text,
                pollOptions = JsonConvert.SerializeObject(options)
            });

            //Saving info that current user create this options
            var list = new List<string>();
            await mobileService.GetTable<Polls>().DeleteAsync(TempPoll.tempPoll);

            foreach (var line in UserPolls.userPolls.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None))
                list.Add(line);
            list.Remove(list.Last());

            list.Remove(TempPoll.tempPoll.Id);

            UserPolls.userPolls = "";

            foreach (var str in list)
                UserPolls.userPolls += str + "\n";

            var users = await mobileService.GetTable<Users>().Where(x => x.userId == mobileService.CurrentUser.UserId).ToListAsync();
            MobileServiceCollection<Polls, Polls> serverData = await mobileService.GetTable<Polls>().ToCollectionAsync();

            users.Last().userPolls += UserPolls.userPolls;
            users.Last().userPolls += serverData.Last().Id + "\n";
            await mobileService.GetTable<Users>().UpdateAsync(users.Last());

           
            
            progressDialog.Cancel();
            StartActivity(typeof(MainActivity));
        }

        //Define a authenticated user.
        private async Task<bool> Authenticate()
        {
            if (mobileService.CurrentUser == null)
            {
                var success = false;
                try
                {
                    // Sign in with Facebook login using a server-managed flow.
                    await mobileService.LoginAsync(this, MobileServiceAuthenticationProvider.MicrosoftAccount);
                    success = true;

                }
                catch (Exception ex)
                {
                    CreateAndShowDialog(ex, "Authentication failed");
                }
                return success;
            }
            else return true;

        }

        private void CreateAndShowDialog(Exception exception, String title)
        {
            CreateAndShowDialog(exception.Message, title);
        }

        private void CreateAndShowDialog(string message, string title)
        {
            var builder = new Android.App.AlertDialog.Builder(this);

            builder.SetMessage(message);
            builder.SetTitle(title);
            builder.Create().Show();
        }
    }

    internal static class TempPoll
    {
        public static Polls tempPoll;
    }
}