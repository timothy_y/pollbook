﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.Design.Widget;

namespace PollBook
{
    [Activity(Label = "Poll_Statistic_Android", MainLauncher = false, Icon = "@drawable/ic_launcher", Theme = "@style/MyTheme")]
    public class SettingActivity : AppCompatActivity
    {
        LinearLayout layout;

        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);

            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            SetTheme(UserSettings.theme);

            SetContentView(Resource.Layout.Settings);

            layout = FindViewById<LinearLayout>(Resource.Id.settingLayout);

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.Settings);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            ListView listView = new ListView(this);


            listView.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, new[] {  "Theme", "Language", "Having a problem?" });

            listView.ItemClick += ListView_ItemClick;
            
            layout.AddView(listView);


        }
        Android.App.AlertDialog alertDialog = null;
        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            switch (e.Position)
            {
                case 0:
                    var builder = new Android.App.AlertDialog.Builder(this);

                    builder.SetTitle("Theme");
                   
                    var tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
                    tempLayout.SetPadding(30, 10, 30, 10);

                    var themes = new RadioGroup(this);
                    themes.AddView(new RadioButton(this) { Text = "Dark", Checked =  UserSettings.theme == Resource.Style.MyTheme? true : false });
                    themes.AddView(new RadioButton(this) { Text = "Light", Checked = UserSettings.theme == Resource.Style.Light? true : false });
                    themes.CheckedChange += Themes_CheckedChange;
                    tempLayout.AddView(themes);

                    alertDialog = builder.Create();
                    alertDialog.SetView(tempLayout);
                    alertDialog.Show();
                    break;
                case 1:

                    break;
            }
        }

        private void Themes_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {

            var themeGroup = sender as RadioGroup;

            var themeButton = alertDialog.FindViewById(e.CheckedId) as RadioButton;

            switch (themeGroup.IndexOfChild(themeButton))
            {
                case 0:
                    UserSettings.theme = Resource.Style.MyTheme;
                    SetTheme(UserSettings.theme);
                    
                    break;
                case 1:
                    UserSettings.theme = Resource.Style.Light;
                    SetTheme(UserSettings.theme);                  
                    break;
                  
            }
            alertDialog.Cancel();
            StartActivity(typeof(SettingActivity));
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {

            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    StartActivity(typeof(MainActivity));

                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }







    }
}