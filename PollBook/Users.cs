using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Security;

namespace PollBook
{
    class Users
    {
        public string Id { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string userPolls { get; set; }
        public string votedPolls { get; set; }
        public string userId { get; set; }
        public string nickname { get; set; }
        public string passwordHash { get; set; }
        public string email { get; set; }
        public string imageUrl { get; set; }
    }
}