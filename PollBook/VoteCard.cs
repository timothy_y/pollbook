using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace PollBook
{
    class VoteCard : CardView
    {
        public int pollPos;

        private PollActivity currentActivity = StaticData.CurrentActivity as PollActivity;

        private TextView title;
        private RadioGroup pollRadioGroup;
        private TextView owner;

        public Poll currentPoll
        {
            get { return this.currentPoll; }
            set
            {
                title.Text = value.pollTitle;
                bool isUserPoll = !(currentActivity is MainActivity || StaticData.CurrentActivity is UserActivity);
                foreach (var option in value.options)
                {
                    var radioButton = new RadioButton(currentActivity) { Text = option.optionsTitle };

                    pollRadioGroup.AddView(radioButton);
                }
                if (!isUserPoll)
                {
                    owner.Text = "By: @" + getOwner(value.ownerId);
                    var userActivity = new Intent(currentActivity, typeof(UserActivity));
                    userActivity.PutExtra("userId", value.ownerId);
                    owner.Click += (sender, args) => currentActivity.StartActivity(userActivity);
                }
                
            }
        }



        public VoteCard(Activity activity) : base(activity)
        {
            this.Elevation = 8f;
            this.Radius = 10f;
            this.UseCompatPadding = true;
            this.LayoutParameters = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MatchParent,
                RecyclerView.LayoutParams.WrapContent);


            bool isUserPoll = !(currentActivity is MainActivity || StaticData.CurrentActivity is UserActivity);
            var layout = new LinearLayout(activity);
            layout.Orientation = Orientation.Vertical;
            layout.SetPadding(20, 20, 20, 20);

            //Adding options title in card        
            var relativeLayout = new RelativeLayout(activity);

            title = new TextView(activity) {TextSize = 20f};

            relativeLayout.AddView(title);

            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            p.AddRule(LayoutRules.AlignParentRight);
            relativeLayout.LayoutParameters = p;

            layout.AddView(relativeLayout);

            if (isUserPoll)
            {
                ImageButton popupButton = currentActivity.LayoutInflater.Inflate(Resource.Menu.settingsButton, null) as ImageButton;
                popupButton.LayoutParameters = p;

                popupButton.Click += PopupButton_Click1;

                relativeLayout.AddView(popupButton);
            }

            //Creatin & adding options radio buttons to card
            pollRadioGroup = new RadioGroup(activity);
          
            pollRadioGroup.CheckedChange += PollRadioGroup_CheckedChange;

            //Adding this all to main feed layout
            layout.AddView(pollRadioGroup);

            if (!isUserPoll)
            {
                owner = new TextView(activity);
               
                owner.SetTextColor(Color.Blue);
                
                layout.AddView(owner);
            }
            this.AddView(layout);
        }

        private string getOwner(string ID)
        {
            var users = currentActivity.users;
            var user = users.Find(x => x.userId == ID);
            return user.nickname;
        }

        private void PopupButton_Click1(object sender, EventArgs e)
        {
            var popupMenu = new Android.Widget.PopupMenu(currentActivity, sender as ImageButton);

            popupMenu.Inflate(Resource.Menu.popupMenu);

            popupMenu.MenuItemClick += PopupMenu_MenuItemClick;

            popupMenu.Show();
        }

        private async void PopupMenu_MenuItemClick(object sender, Android.Widget.PopupMenu.MenuItemClickEventArgs e)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.popup_delete:

                    await currentActivity.getServerData();

                    foreach (Polls poll in currentActivity.serverData)
                        if (poll.Id == currentActivity.serverData[pollPos].Id)
                        {
                            await currentActivity.mobileService.GetTable<Polls>().DeleteAsync(poll);
                        }

                    await currentActivity.removeUserPoll(currentActivity.serverData[pollPos].Id);

                    currentActivity.serverData.RemoveAt(pollPos);
                    currentActivity.ShowFeedAsync();
                    break;
                case Resource.Id.popup_edit:
                    var editActivity = new Intent(currentActivity, typeof(EditActivity));
                    editActivity.PutExtra("poll", JsonConvert.SerializeObject(currentPoll));
                    currentActivity.StartActivity(editActivity);
                    break;

            }
        }

        private async void PollRadioGroup_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            //Getting access to clicked radio button (polls option)
            RadioGroup radioGroup = sender as RadioGroup;

            RadioButton radioButton = FindViewById(e.CheckedId) as RadioButton;

            //Getting indices of cliked options option
            int pollIndex = pollPos;
            int optionIndex = radioGroup.IndexOfChild(radioButton);

            int index = 0;
            if (!currentActivity.tryGetUserVote(currentActivity.serverData[pollPos].Id, ref index))//Fixing bug with several taps 
            {             
                currentActivity.pollData[pollPos].Vote(optionIndex);

                currentActivity.adapter.NotifyItemChanged(pollPos);

                //Sending info to server
                await currentActivity.updateOptionRate(currentActivity.serverData[pollIndex].Id, optionIndex);

                await currentActivity.getServerData();
            }
        }
    }
}