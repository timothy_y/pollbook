﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.Design.Widget;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;


namespace PollBook
{
    /// <summary>
    /// Add options page activity class
    /// </summary>
    [Activity(Label = "Poll_Statistic_Android", MainLauncher = false, Icon = "@drawable/ic_launcher", Theme = "@style/MyTheme")]
    public class AddPageActivity : AppCompatActivity
    {
        //Var for work with server part
        private static MobileServiceClient mobileService = CurrentUser.mobileService;

        //Layouts and views
        private LinearLayout layout;
        private EditText pollTitleEdit;
        private List<EditText> pollOptionsEdit = new List<EditText>();
        private LinearLayout optionsLayout;
        private CardView optionsCardView;
        private int optionsCount = 2;

        /// <summary>
        /// Main function. Runs automatically on app start
        /// </summary>
        /// <param name="bundle"></param>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            SetTheme(UserSettings.theme);

            SetContentView(Resource.Layout.AddPoll);

            //Getting access for main layout
            layout = FindViewById<LinearLayout>(Resource.Id.addPage_layout);
            layout.SetPadding(50, 50, 50, 50);

            //Getting access for main toolbar
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = "Add new options";
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            

            //Creating card with options title text field
            CardView cardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };
            LinearLayout tempLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            tempLayout.SetPadding(20, 20, 20, 20);

            //Creating card with 2 default options options text fields
            tempLayout.AddView(new TextView(this) { Text = GetString(Resource.String.titleInput), TextSize = 20 });
            pollTitleEdit = new EditText(this) { Hint = GetString(Resource.String.titleHint), InputType = Android.Text.InputTypes.ClassText };
            
            //Adding this all to main layout
            tempLayout.AddView(pollTitleEdit);
            cardView.AddView(tempLayout);
            layout.AddView(cardView);


            optionsCardView = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };
            optionsLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            optionsLayout.SetPadding(20, 20, 20, 20);

            optionsLayout.AddView(new TextView(this) { Text = GetString(Resource.String.optionsInput), TextSize = 20 });
            pollOptionsEdit.Add(new EditText(this) { Hint = GetString(Resource.String.optionsHint), InputType = Android.Text.InputTypes.ClassText });
            optionsLayout.AddView(pollOptionsEdit.Last());
            pollOptionsEdit.Add(new EditText(this) { Hint = GetString(Resource.String.optionsHint), InputType = Android.Text.InputTypes.ClassText });
            optionsLayout.AddView(pollOptionsEdit.Last());

            //Adding this all to main layout
            optionsCardView.AddView(optionsLayout);
            layout.AddView(optionsCardView);


            //Creating card with options settings
            CardView settingsCard = new CardView(this) { CardElevation = 8f, Radius = 10f, UseCompatPadding = true };
            LinearLayout settingsLayout = new LinearLayout(this) { Orientation = Orientation.Vertical };
            settingsLayout.SetPadding(20, 20, 20, 20);

            settingsLayout.AddView(new TextView(this) { Text = GetString(Resource.String.optionsSettings), TextSize = 20 });

            Android.Widget.Switch anonimSwitch = new Android.Widget.Switch(this) { Text = GetString(Resource.String.anonimPoll), TextSize = 15 };
            anonimSwitch.SetTextColor(Android.Graphics.Color.LightGray);
            settingsLayout.AddView(anonimSwitch);

            Android.Widget.Switch reVoteSwitch = new Android.Widget.Switch(this) { Text = GetString(Resource.String.revoteOption), TextSize = 15 };
            reVoteSwitch.SetTextColor(Android.Graphics.Color.LightGray);
            settingsLayout.AddView(reVoteSwitch);          

            //Adding this all to main layout
            settingsCard.AddView(settingsLayout);
            layout.AddView(settingsCard);


        }

        /// <summary>
        /// Filling toolbar with buttons from .xml file
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.addPage_top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        /// <summary>
        /// Handling toolbar buttons clicks
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                   StartActivity(typeof(MainActivity));
                    break;
                case Resource.Id.menu_add:
                    pollOptionsEdit.Add(new EditText(this) { Hint = GetString(Resource.String.optionsHint), InputType = Android.Text.InputTypes.ClassText });
                    optionsCount++;
                    optionsLayout.AddView(pollOptionsEdit.Last());
                    break;
                case Resource.Id.menu_remove:
                    if (!pollOptionsEdit.Last().Equals(pollOptionsEdit.First()))
                    {                       
                        optionsLayout.RemoveView(pollOptionsEdit.Last());    
                        pollOptionsEdit.Remove(pollOptionsEdit.Last());
                        optionsCount--;
                    }          
                    break;
                case Resource.Id.menu_send:

                    sendPoll();    
                    
                    return true;
                    
            }
            return base.OnOptionsItemSelected(item);
        }

        /// <summary>
        /// Sending options info to server
        /// </summary>
        private async void sendPoll()
        {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.SetTitle(GetString(Resource.String.Send));
            progressDialog.SetCancelable(false);
            progressDialog.SetCanceledOnTouchOutside(false);
            progressDialog.Show();

            var options = new List<Option>();

            //Converting all options titles into one string to save it on server
            pollOptionsEdit.ForEach(x =>
            {
                options.Add(new Option()
                {
                    optionsTitle = x.Text,
                    optionRate = 0
                });
            } );
            
            
            //Sending data to server               
            await mobileService.GetTable<Polls>().InsertAsync(new Polls
            {
                pollTitle = pollTitleEdit.Text,
                pollOptions = JsonConvert.SerializeObject(options),
                ownerId = JsonConvert.DeserializeObject<Users>(UserSettings.User).userId
            });

            //Saving info that current user create this options
            List<Polls> serverData = await mobileService.GetTable<Polls>().ToListAsync();
            await StaticData.CurrentActivity.addUserPoll(serverData.Last().Id);
            StartActivity(typeof(MainActivity));
        }
       
    }

  
}