using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace PollBook
{
    class StatisticCard : CardView
    {
        public int pollPos;
        private PollActivity currentActivity = StaticData.CurrentActivity as PollActivity;

        private TextView title;
        private RadioGroup pollRadioGroup;
        private TextView owner;

        private LinearLayout layout;

        public Poll currentPoll
        {
            get { return this.currentPoll; }
            set
            {
                title.Text = value.pollTitle;
                bool isUserPoll = !(currentActivity is MainActivity || StaticData.CurrentActivity is UserActivity);
                //Creatin & adding progress bars to card
                //Each progress bar shows options option rating
                foreach (var option in value.options)
                {
                    //Using relative layout to overlay progress(rating) bar and option title
                    RelativeLayout tempLayout = new RelativeLayout(currentActivity);
                    tempLayout.LayoutParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);

                    ProgressBar progressBar = currentActivity.LayoutInflater.Inflate(Resource.Menu.progress_bar, null) as ProgressBar;

                    progressBar.Progress = option.optionRate;
                    progressBar.Max = value.GetRate();

                    progressBar.LongClick += ProgressBar_LongClick;

                    int index = 0;
                    currentActivity.tryGetUserVote(value.ID, ref index);

                    //if (j == index)
                    //    progressBar.ProgressDrawable.SetColorFilter(Color.ParseColor("#2196F3"), PorterDuff.Mode.Multiply);

                    progressBar.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.MatchParent);
                    progressBar.ScaleY = 8f;
                    progressBar.SetPadding(0, reletivePaddingHeight(), 0, reletivePaddingHeight());
                    tempLayout.AddView(progressBar);

                    //Adding otion title on progress bar
                    TextView text = new TextView(currentActivity);
                    text.Text = option.optionsTitle;
                    text.SetTextColor(Android.Graphics.Color.Black);
                    text.SetPaddingRelative(10, 10, 0, 0);
                    text.TextSize = reletiveTextSize();
                    text.TextAlignment = TextAlignment.Center;

                    tempLayout.AddView(text);
                    layout.AddView(tempLayout);
                }

                if (!isUserPoll)
                {
                    owner = new TextView(currentActivity);

                    owner.SetTextColor(Color.Blue);
                    owner.Text = "By: @" + getOwner(value.ownerId);
                    var userActivity = new Intent(currentActivity, typeof(UserActivity));
                    userActivity.PutExtra("userId", value.ownerId);
                    owner.Click += (sender, args) => currentActivity.StartActivity(userActivity);
                    layout.AddView(owner);
                }
               
            }
        }

        public StatisticCard(Activity activity) : base(activity)
        {
            this.Elevation = 8f;
            this.Radius = 10f;
            this.UseCompatPadding = true;
            this.LayoutParameters = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MatchParent,
                RecyclerView.LayoutParams.WrapContent);

            bool isUserPoll = !(currentActivity is MainActivity || StaticData.CurrentActivity is UserActivity);
            layout = new LinearLayout(activity);
            layout.Orientation = Orientation.Vertical;
            layout.SetPadding(20, 20, 20, 20);


            //Adding options title in card        
            var relativeLayout = new RelativeLayout(activity);
            title = new TextView(activity) { TextSize = 20f };

            relativeLayout.AddView(title);

            RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            p.AddRule(LayoutRules.AlignParentRight);
            relativeLayout.LayoutParameters = p;

            layout.AddView(relativeLayout);

            if (isUserPoll)
            {
                ImageButton popupButton = currentActivity.LayoutInflater.Inflate(Resource.Menu.settingsButton, null) as ImageButton;
                popupButton.LayoutParameters = p;

                popupButton.Click += PopupButton_Click1;

                relativeLayout.AddView(popupButton);
            }
          

           
            this.AddView(layout);
        }

        private string getOwner(string ID)
        {
            var users = currentActivity.users;
            var user = users.Find(x => x.userId == ID);
            return user.nickname;
        }

        private void PopupButton_Click1(object sender, EventArgs e)
        {
            var popupMenu = new Android.Widget.PopupMenu(currentActivity, sender as ImageButton);

            popupMenu.Inflate(Resource.Menu.popupMenu);

            popupMenu.MenuItemClick += PopupMenu_MenuItemClick;

            popupMenu.Show();
        }

        private async void PopupMenu_MenuItemClick(object sender, Android.Widget.PopupMenu.MenuItemClickEventArgs e)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.popup_delete:

                    await currentActivity.getServerData();

                    foreach (Polls poll in currentActivity.serverData)
                        if (poll.Id == currentActivity.serverData[pollPos].Id)
                        {
                            await currentActivity.mobileService.GetTable<Polls>().DeleteAsync(poll);
                        }

                    await currentActivity.removeUserPoll(currentActivity.serverData[pollPos].Id);

                    currentActivity.serverData.RemoveAt(pollPos);
                    currentActivity.ShowFeedAsync();
                    break;
                case Resource.Id.popup_edit:
                    var editActivity = new Intent(currentActivity, typeof(EditActivity));
                    editActivity.PutExtra("poll", JsonConvert.SerializeObject(currentPoll));
                    currentActivity.StartActivity(editActivity);
                    break;

            }
        }

        private async void ProgressBar_LongClick(object sender, LongClickEventArgs e)
        {         
            int pollIndex = pollPos;
            
            var poll = currentActivity.pollData[pollIndex];

            int index = 0;
            currentActivity.tryGetUserVote(poll.ID, ref index);
         
            currentActivity.pollData[pollIndex].CancelVote(index);

            currentActivity.adapter.NotifyItemChanged(pollPos);

            //Sending info to server
            await currentActivity.mobileService.GetTable<Polls>().UpdateAsync(new Polls()
            {
                Id = poll.ID,
                ownerId = poll.ownerId,
                pollOptions = JsonConvert.SerializeObject(poll.options),
                pollRate = poll.pollRate,
                pollTitle = poll.pollTitle
            });

            await currentActivity.removeVotedPoll(poll.ID);

            await currentActivity.getServerData();
        }

        private int reletivePaddingHeight()
        {
            var dp = (int)((Resources.DisplayMetrics.Ydpi * 25) / 320);
            return dp;
        }
        private int reletiveTextSize()
        {
            var dp = (int)((Resources.DisplayMetrics.WidthPixels * 25) / 2560);
            return dp;
        }
    }
}