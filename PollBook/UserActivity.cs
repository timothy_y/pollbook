using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace PollBook
{
    [Activity(Label = "UserActivity", MainLauncher = false, Icon = "@drawable/ic_launcher", Theme = "@style/MyTheme")]
    class UserActivity : PollActivity
    {
        //Layouts
        private LinearLayout layout;
        public string userId;
        private Users user;
        /// <summary>
        /// Main function. Runs automatically on app start
        /// </summary>
        /// <param name="bundle"></param>
        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            userId = Intent.GetStringExtra("userId");

            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            SetTheme(UserSettings.theme);

            SetContentView(Resource.Layout.Profile);

            StaticData.CurrentActivity = this;

            //Getting access for main layout
            layout = FindViewById<LinearLayout>(Resource.Id.profile_layout);


            //Getting access for main toolbar
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.Profile);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);

            //Getting access for recyclerView
            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            pollData = new List<Poll>();
            adapter = new PollAdapter(this, pollData);
            recyclerView.SetAdapter(adapter);


            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(layoutManager);

            //Getting access for refresher
            refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            refresher.SetColorSchemeColors(Resource.Color.darkblue,
                                      Resource.Color.purple,
                                      Resource.Color.orange,
                                      Resource.Color.green);
            refresher.Refresh += async delegate
            {
                await getServerData();
                refresher.Refreshing = false;
                ShowFeedAsync();
            };

            var userList = await mobileService.GetTable<Users>().ToListAsync();
            user = userList.Find(x => x.userId == userId);

            var nameView = FindViewById<TextView>(Resource.Id.textView);
            nameView.Text = user.firstName + " " + user.secondName;

            var profileImage = FindViewById<ImageView>(Resource.Id.profileImage);
            if (JsonConvert.DeserializeObject<Users>(UserSettings.User).imageUrl != "")
            {
                profileImage.SetImageBitmap(GetImageBitmapFromUrl(JsonConvert.DeserializeObject<Users>(UserSettings.User).imageUrl));
            }

            //Sync data with server & show feed (async method)
            syncData();

        }

        /// <summary>
        /// Filling toolbar with buttons from .xml file
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.feed_top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        /// <summary>
        /// Showing cards with options on feed layout
        /// </summary>
        /// <returns></returns>
        public override void ShowFeedAsync()
        {
            pollData.Clear();
            foreach (var dataCollum in serverData)
            {
                Poll currentPoll = new Poll();
             

                var userPolls = JsonConvert.DeserializeObject<List<string>>(user.userPolls);

                if (userPolls.Find(x => x == dataCollum.Id) != null)
                {
                    //Parsing data
                    currentPoll.pollTitle = dataCollum.pollTitle;
                    currentPoll.pollRate = dataCollum.pollRate;
                    currentPoll.ID = dataCollum.Id;
                    currentPoll.options = new List<Option>();
                    currentPoll.ownerId = dataCollum.ownerId;

                    int index = 0;
                    if (tryGetUserVote(currentPoll.ID, ref index))
                        currentPoll.isVoted = true;

                    //Convering string that contains options options titles into list
                    foreach (var option in JsonConvert.DeserializeObject<List<Option>>(dataCollum.pollOptions))
                    {
                        currentPoll.options.Add(option);
                    }
                    pollData.Add(currentPoll);
                }
            }
            adapter = new PollAdapter(this, pollData);
            recyclerView.SetAdapter(adapter);
            adapter.NotifyDataSetChanged();
        }

        /// <summary>
        /// Handling toolbar buttons clicks
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    StartActivity(typeof(MainActivity));
                    break;
                case Resource.Id.menu_search:

                    var searchEdit = new EditText(this);
                    Android.App.AlertDialog alertDialog;
                    var builder = new Android.App.AlertDialog.Builder(this);
                    builder.SetTitle(GetString(Resource.String.Search)).SetPositiveButton("OK", (senderAlert, args) => Search(searchEdit.Text)).SetNegativeButton(GetString(Resource.String.cancel), (senderAlert, args) => alertDialog = null); ;

                    alertDialog = builder.Create();
                    alertDialog.SetView(searchEdit);
                    alertDialog.Show();
                    break;
                case Resource.Id.menu_preferences:
                    StartActivity(typeof(SettingActivity));
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }

    }

}
