﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PollBook
{

    public class Polls
    {
        public string Id { get; set; }
        public string pollTitle { get; set; }
        public int pollRate { get; set; }
        public string pollOptions { get; set; }
        public string ownerId { get; set; }
    }
}
