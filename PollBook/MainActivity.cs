﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Xamarin.Facebook;


namespace PollBook
{
    [Activity(Icon = "@drawable/ic_launcher", Label = "PollBook", MainLauncher = true, Theme = "@style/MyTheme")]
    class MainActivity : PollActivity
    {

        /// <summary>
        /// Main function. Runs automatically on app start
        /// </summary>
        /// <param name="bundle"></param>
        protected override void OnCreate(Bundle bundle)
        {
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            SetTheme(UserSettings.theme);
           

            StaticData.CurrentActivity = this;

            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            //Getting access for main toolbar
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu_white_24dp);
            SupportActionBar.Title = GetString(Resource.String.Feed);

            //Getting access for left nav bar
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            navigationView.NavigationItemSelected += OnNavigationItemSelected;

            //Getting access for recyclerView
            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            pollData = new List<Poll>();
            adapter = new PollAdapter(this, pollData);
            recyclerView.SetAdapter(adapter);


            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.Vertical, false);
            recyclerView.SetLayoutManager(layoutManager);

            //Getting access for refresher
            refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            refresher.SetColorSchemeColors(Resource.Color.darkblue,
                                      Resource.Color.purple,
                                      Resource.Color.orange,
                                      Resource.Color.green);
            refresher.Refresh += async delegate
            {
                await getServerData();
                refresher.Refreshing = false;
                ShowFeedAsync();
            };
            //Getting access for floatin "+" button
            FloatingActionButton fabButton = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fabButton.Click += (sender, args) => StartActivity(typeof(AddPageActivity));

            //Sync data with server & show feed (async method)
            if (UserSettings.User == "")
                StartActivity(typeof(LoginActivity));
            else
                syncData();
        }

        /// <summary>
        /// Filling toolbar with buttons from .xml file
        /// </summary>
        /// <param name="menu"></param>
        /// <returns></returns>
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.feed_top_menus, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        /// <summary>
        /// Handling left nav bar buttons clicks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            IMenuItem menuItem = e.MenuItem;
            menuItem.SetChecked(!menuItem.IsChecked);
            switch (menuItem.ItemId)
            {
                case Resource.Id.nav_profile:
                    StartActivity(typeof(ProfileActivity));
                    break;
                case Resource.Id.nav_add:
                    StartActivity(typeof(AddPageActivity));
                    break;
                case Resource.Id.nav_settings:
                    StartActivity(typeof(SettingActivity));
                    break;
                case Resource.Id.nav_About:
                    StartActivity(typeof(AboutActivity));
                    break;
            }
        }

        /// <summary>
        /// Handling toolbar buttons clicks
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    if (drawerLayout.IsDrawerOpen(Android.Support.V4.View.GravityCompat.Start))
                    {
                        drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
                        break;
                    }
                    this.drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    break;
                case Resource.Id.menu_search:

                    var searchEdit = new EditText(this);
                    Android.App.AlertDialog alertDialog;
                    var builder = new Android.App.AlertDialog.Builder(this);
                    builder.SetTitle(GetString(Resource.String.Search)).SetPositiveButton("OK", (senderAlert, args) => Search(searchEdit.Text)).SetNegativeButton(GetString(Resource.String.cancel), (senderAlert, args) => alertDialog = null); ;

                    alertDialog = builder.Create();
                    alertDialog.SetView(searchEdit);
                    alertDialog.Show();
                    break;
                case Resource.Id.menu_preferences:
                    StartActivity(typeof(SettingActivity));
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

    }

}

