using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace PollBook
{
    class PollAdapter : RecyclerView.Adapter
    {

        Activity activity;
        private List<Poll> polls;

        private const int voteType = 0;
        private const int statType = 1;

        public PollAdapter(Activity activity, List<Poll> polls)
        {
            this.polls = polls;
            this.activity = activity;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            switch (viewType)
            {
                case voteType:
                    return new AdapterViewHolder(new VoteCard(activity));                  
                case statType:
                    return new AdapterViewHolder(new StatisticCard(activity));                
                default:
                    return new AdapterViewHolder(new VoteCard(activity));
            }
           
        }

        public override int GetItemViewType(int position)
        {
            return polls[position].isVoted ? statType : voteType;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = polls[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as AdapterViewHolder;
            switch (GetItemViewType(position))
            {
                case voteType:
                    (holder.cardView as VoteCard).currentPoll = item;
                    (holder.cardView as VoteCard).pollPos = position;
                    break;

                case statType:
                    (holder.cardView as StatisticCard).currentPoll = item;
                    (holder.cardView as StatisticCard).pollPos = position;
                    break;
            }
        }


        public override int ItemCount => polls.Count;
    }
}