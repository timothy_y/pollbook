using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Gms.Plus;
using Android.Gms.Plus.Model.People;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Auth;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;

namespace PollBook
{
    [Activity(Label = "Log In")]
    public class LoginActivity : Activity, IFacebookCallback, GoogleApiClient.IConnectionCallbacks, GoogleApiClient.IOnConnectionFailedListener
    {
        private EditText loginBox;
        private EditText passwordBox;
        private Button loginButton;
        private LoginButton facebookButton;
        private SignInButton googleButton;
        private Button singInButton;
        private Bundle bundle;

        private bool isGoogleAuth;

        public MobileServiceClient mobileService = CurrentUser.mobileService;
      

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Login);

            FacebookSdk.SdkInitialize(this.ApplicationContext);

            bundle = savedInstanceState;

            loginBox = FindViewById<EditText>(Resource.Id.loginField);
            passwordBox = FindViewById<EditText>(Resource.Id.passwordField);
            loginButton = FindViewById<Button>(Resource.Id.loginButton);
            facebookButton = FindViewById<LoginButton>(Resource.Id.facebookButton);
            googleButton = FindViewById<SignInButton>(Resource.Id.googleButton);
            singInButton = FindViewById<Button>(Resource.Id.signInButton);


            singInButton.Click += (sender, args) => StartActivity(typeof(SinginActivity));
            loginButton.Click += LoginButton_Click;

            googleButton.Click += GoogleButton_Click;
            mGoogleApiClient = new GoogleApiClient.Builder(this)
              .AddConnectionCallbacks(this)
              .AddOnConnectionFailedListener(this)
              .AddApi(PlusClass.API)
              .AddScope(new Scope(Scopes.Profile))
              .Build();

            facebookButton.SetReadPermissions(new List<string>
            {
                "user_friends",
                "public_profile"
            });
            facebookButton.Click += (sender, args) => isGoogleAuth = false;

            callManager = CallbackManagerFactory.Create();
            facebookButton.RegisterCallback(callManager, this);
            profileTracker = new MyProfileTracker();
            profileTracker.mOnProfileChanged += ProfileTracker_mOnProfileChanged;
            profileTracker.StartTracking();
        }    

        private async void LoginButton_Click(object sender, EventArgs e)
        {
            var users = await mobileService.GetTable<Users>().Where(x => x.email == loginBox.Text).ToListAsync();
            if (users.Count != 0)
            {
                var user = users.Last();
                if (user.passwordHash == passwordBox.Text.GetHashCode().ToString())
                {
                    UserSettings.User = JsonConvert.SerializeObject(user);
                    StartActivity(typeof(MainActivity));                   
                }
                else
                {
                    Toast.MakeText(this, "Incorrect password", ToastLength.Short).Show();
                }
            }
            else
            {
                Toast.MakeText(this, "Incorrect email", ToastLength.Short).Show();
            }
        }

        ///Facebook auth 
        public void OnCancel() { }
        public void OnError(FacebookException p0) { }
        public void OnSuccess(Java.Lang.Object result) { }
        public ICallbackManager callManager;
        private MyProfileTracker profileTracker;

        protected override void OnActivityResult(int requestCode, Result resultCode, Android.Content.Intent data)
        {           
            if (isGoogleAuth)
            {
                if (requestCode == 0)
                {
                    if (resultCode != Result.Ok)
                    {
                        mSignInClicked = false;
                    }

                    mIntentInProgress = false;

                    if (!mGoogleApiClient.IsConnecting)
                    {
                        mGoogleApiClient.Connect();
                    }
                }
            }
            else
            {
                base.OnActivityResult(requestCode, resultCode, data);
                callManager.OnActivityResult(requestCode, (int)resultCode, data);
            }
        }

        private async void ProfileTracker_mOnProfileChanged(object sender, OnProfileChangedEventArgs e)
        {
            if (e.mProfile != null)
            {
                try
                {
                    var users =
                        await mobileService.GetTable<Users>().Where(x => x.userId == e.mProfile.Id).ToListAsync();
                    if (users.Count == 0)
                    {                      
                        var nickBox = new EditText(this){Hint = "Nickname"};
                        Android.App.AlertDialog alertDialog;
                        var builder = new Android.App.AlertDialog.Builder(this);
                        builder.SetTitle("Input nickname").SetPositiveButton("OK", async (senderAlert, args) =>
                        {
                            ProgressDialog progressDialog = new ProgressDialog(this);
                            progressDialog.SetTitle("Processing");
                            progressDialog.Show();

                            var user = new Users()
                            {
                                email = "facebook" + e.mProfile.Id,
                                passwordHash = ("facebook" + e.mProfile.Id).GetHashCode().ToString(),
                                nickname = nickBox.Text,
                                firstName = e.mProfile.FirstName,
                                secondName = e.mProfile.LastName,
                                userId = e.mProfile.Id,
                                userPolls = "",
                                votedPolls = "",
                                imageUrl = "http://graph.facebook.com/" + e.mProfile.Id + "/picture?type=large" 
                            };

                            await mobileService.GetTable<Users>().InsertAsync(user);
                            UserSettings.User = JsonConvert.SerializeObject(user);
                            StartActivity(typeof(MainActivity));

                            progressDialog.Cancel();
                        }); 
                        alertDialog = builder.Create();
                        alertDialog.SetView(nickBox);
                        alertDialog.Show();

                       
                    }
                    else
                    {
                        UserSettings.User = JsonConvert.SerializeObject(users.Last());
                        StartActivity(typeof(MainActivity));
                    }
                    
                }
                catch (Java.Lang.Exception ex)
                {
                }
            }
        }

        public class MyProfileTracker : ProfileTracker
        {
            public event EventHandler<OnProfileChangedEventArgs> mOnProfileChanged;
            protected override void OnCurrentProfileChanged(Profile oldProfile, Profile newProfile)
            {
                if (mOnProfileChanged != null)
                {
                    mOnProfileChanged.Invoke(this, new OnProfileChangedEventArgs(newProfile));
                }
            }
        }

        public class OnProfileChangedEventArgs : EventArgs
        {
            public Profile mProfile;
            public OnProfileChangedEventArgs(Profile profile)
            {
                mProfile = profile;
            }
        }

        //Google+ auth
        private GoogleApiClient mGoogleApiClient;
        private ConnectionResult mConnectionResult;
        private bool mIntentInProgress;
        private bool mSignInClicked;
        private bool mInfoPopulated;

        private void GoogleButton_Click(object sender, EventArgs e)
        {
            isGoogleAuth = true;
            if (!mGoogleApiClient.IsConnecting)
            {
                mSignInClicked = true;
                ResolveSignInError();
            }
        }

        private void ResolveSignInError()
        {
            if (mGoogleApiClient.IsConnected)
            {
                //No need to resolve errors, already connected
                return;
            }

            if (mConnectionResult.HasResolution)
            {
                try
                {
                    mIntentInProgress = true;
                    StartIntentSenderForResult(mConnectionResult.Resolution.IntentSender, 0, null, 0, 0, 0);
                }

                catch (Android.Content.IntentSender.SendIntentException e)
                {
                    Console.WriteLine(e.ToString());
                    //The intent was cancelled before it was sent. Return to the default
                    //state and attempt to connect to get an updated ConnectionResult
                    mIntentInProgress = false;
                    mGoogleApiClient.Connect();
                }
            }
        }

        public async void OnConnected(Bundle connectionHint)
        {
            mSignInClicked = false;

            if (PlusClass.PeopleApi.GetCurrentPerson(mGoogleApiClient) != null)
            {
                IPerson plusPerson = PlusClass.PeopleApi.GetCurrentPerson(mGoogleApiClient);

                var users =
                        await mobileService.GetTable<Users>().Where(x => x.userId == plusPerson.Id).ToListAsync();
                if (users.Count == 0)
                {
                    var nickBox = new EditText(this) { Hint = "Nickname" };
                    Android.App.AlertDialog alertDialog;
                    var builder = new Android.App.AlertDialog.Builder(this);
                    builder.SetTitle("Input nickname").SetPositiveButton("OK", async (senderAlert, args) =>
                    {
                        ProgressDialog progressDialog = new ProgressDialog(this);
                        progressDialog.SetTitle("Processing");
                        progressDialog.Show();

                        var user = new Users()
                        {
                            email = "google" + plusPerson.Id,
                            passwordHash = ("google" + plusPerson.Id).GetHashCode().ToString(),
                            nickname = nickBox.Text,
                            firstName = plusPerson.Name.GivenName,
                            secondName = plusPerson.Name.FamilyName,
                            userId = plusPerson.Id,
                            userPolls = "",
                            votedPolls = "",
                            imageUrl = plusPerson.HasImage ? plusPerson.Image.Url : "https://i.imgur.com/HXiij9f.jpg"
                        };

                        await mobileService.GetTable<Users>().InsertAsync(user);
                        UserSettings.User = JsonConvert.SerializeObject(user);
                        StartActivity(typeof(MainActivity));

                        progressDialog.Cancel();
                    });
                    alertDialog = builder.Create();
                    alertDialog.SetView(nickBox);
                    alertDialog.Show();
                }
                else
                {
                    UserSettings.User = JsonConvert.SerializeObject(users.Last());
                    StartActivity(typeof(MainActivity));
                }
            }
        }

        protected override void OnStart()
        {
            base.OnStart();
            mGoogleApiClient.Connect();
        }

        protected override void OnStop()
        {
            base.OnStop();
            if (mGoogleApiClient.IsConnected)
            {
                mGoogleApiClient.Disconnect();
            }
        }

        public void OnConnectionSuspended(int cause)
        {

        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            if (!mIntentInProgress)
            {
                //Store the ConnectionResult so that we can use it later when the user clicks 'sign-in;
                mConnectionResult = result;

                if (mSignInClicked)
                {
                    //The user has already clicked 'sign-in' so we attempt to resolve all
                    //errors until the user is signed in, or the cancel
                    ResolveSignInError();
                }
            }
        }

        public async Task<string> UploadImage(Stream source)
        {
            var client = new ImgurClient(GetString(Resource.String.imgurClientId), GetString(Resource.String.imgurClientSecret));
            var endpoint = new ImageEndpoint(client);

            try
            {
                var image = await endpoint.UploadImageStreamAsync(source);

                return image.Link;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            };
        }
    }
}