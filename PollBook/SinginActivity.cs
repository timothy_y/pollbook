using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace PollBook
{
    [Activity(Label = "Sing In")]
    public class SinginActivity : Activity
    {
        private Button loginButton;
        private EditText passwordBox;
        private EditText confirmPasswordBox;
        private EditText emailBox;
        private TextView passwordError;
        private TextView confirmPasswordError;
        private TextView emailError;

        private MobileServiceClient mobileService = CurrentUser.mobileService;

        private bool isFirstStep = true;

        private Users user;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.SingIn);


            loginButton = FindViewById<Button>(Resource.Id.loginButton);

            passwordBox = FindViewById<EditText>(Resource.Id.passwordField);

            confirmPasswordBox = FindViewById<EditText>(Resource.Id.confirmPasswordField);

            emailBox = FindViewById<EditText>(Resource.Id.loginField);

            passwordError = FindViewById<TextView>(Resource.Id.passwordError);
            passwordError.SetTextColor(Color.Red);

            emailError = FindViewById<TextView>(Resource.Id.emailError);
            emailError.SetTextColor(Color.Red);

            confirmPasswordError = FindViewById<TextView>(Resource.Id.confirmPasswordError);
            confirmPasswordError.SetTextColor(Color.Red);

            loginButton.Click += LoginButton_Click;
        }

        private async void LoginButton_Click(object sender, EventArgs e)
        {
            if (isFirstStep)
            {
                if (passwordBox.Text.Length < 6)
                {
                    passwordError.Text = "Less than 6 symbols";

                }
                else if (emailBox.Text == "")
                {
                    emailError.Text = "Input email";
                }
                else if (passwordBox.Text != confirmPasswordBox.Text)
                {
                    confirmPasswordError.Text = "Passwords does not match";
                }
                else
                {
                    isFirstStep = false;                  
                    if (!await checkEmail(emailBox.Text))
                    {
                        Toast.MakeText(this, "One more step...", ToastLength.Short).Show();
                        user = new Users()
                        {
                            email = emailBox.Text,
                            passwordHash = passwordBox.Text.GetHashCode().ToString(),
                            userPolls = "",
                            votedPolls = "",
                            imageUrl = "https://i.imgur.com/HXiij9f.jpg"
                        };
                        nextStep();
                    }
                    else
                    {
                        emailError.Text = "Email already used";
                    }
                    
                }
            }
            else
            {
                if (!await checkNickname(confirmPasswordBox.Text))
                {
                    user.firstName = emailBox.Text;
                    user.secondName = passwordBox.Text;
                    user.nickname = confirmPasswordBox.Text;
                    user.userId = confirmPasswordBox.Text.GetHashCode().ToString();

                    ProgressDialog progressDialog = new ProgressDialog(this);
                    progressDialog.SetTitle("Processing");
                    progressDialog.Show();

                    await mobileService.GetTable<Users>().InsertAsync(user);

                    UserSettings.User = JsonConvert.SerializeObject(user);
                    StartActivity(typeof(MainActivity));

                    progressDialog.Cancel();
                }
                else
                {
                    confirmPasswordError.Text = "Nickname already used";
                }               
            }
        }

        private void nextStep()
        {
            emailBox.Hint = "First name";
            emailBox.Text = "";
            passwordBox.Hint = "Last name";
            passwordBox.InputType = InputTypes.Null;
            passwordBox.Text = "";
            confirmPasswordBox.Hint = "Nickname";
            confirmPasswordBox.Text = "";
            confirmPasswordBox.InputType = InputTypes.Null;
        }

        private async Task<bool> checkEmail(string email)
        {
            var users = await mobileService.GetTable<Users>().ToListAsync();
            return users.Find(x => x.email == email) != null ? true : false;
        }

        private async Task<bool> checkNickname(string nick)
        {
            var users = await mobileService.GetTable<Users>().ToListAsync();
            return users.Find(x => x.nickname == nick) != null ? true : false;
        }
    }
}