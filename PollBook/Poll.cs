﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PollBook
{

    class Poll
    {     
        public List<Option> options;
        public string pollTitle; 
        public int pollRate = 0;
        public bool isVoted = false;
        public string ID;
        public string ownerId;
        public Poll()
        {
            options = new List<Option>();
        }

        public void setPollTitle(string title)
        {
            pollTitle = title;
        }

        public void AddItem(string text)
        {
            options.Add(new Option(){ optionRate = 0, optionsTitle = text });
        }

        public void RemoveItem(string text)
        {
            options.Remove(options.Find(x => x.optionsTitle.Equals(text)));
        }

        public void Vote(int index)
        {
            options[index].optionRate++;
            pollRate++;
            isVoted = true;
        }

        public void CancelVote(int index)
        {
            options[index].optionRate--;
            pollRate--;
            isVoted = false;
        }

        public int GetRate()
        {
            return pollRate;
        }
    }
}
